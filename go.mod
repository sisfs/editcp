module github.com/dalefarnsworth-dmr/editcp

go 1.12

require (
	github.com/dalefarnsworth-dmr/codeplug v1.0.26
	github.com/dalefarnsworth-dmr/debug v1.0.20
	github.com/dalefarnsworth-dmr/dfu v1.0.20
	github.com/dalefarnsworth-dmr/ui v1.0.19
	github.com/dalefarnsworth-dmr/userdb v1.0.26
	github.com/gopherjs/gopherjs v0.0.0-20211014092231-cac8ae8ec3c8 // indirect
	github.com/therecipe/qt v0.0.0-20200904063919-c0c124a5770d
)
